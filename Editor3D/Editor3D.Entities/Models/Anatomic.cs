﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Editor3D.Entities.Models
{
    public class Anatomic
    {
        [Key]
        public int AnatomicId { get; set; }
        public string Name { get; set; }
        public ICollection<Scene> Scenes { get; set; }

    }
}