﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Editor3D.Entities.Models
{
    public class Scene
    {
        [Key]
        public int SceneId { get; set; }
        public DateTime Time { get; set; }

        
        public Vendor Vendor { get; set; }
        public Anatomic Anatomic { get; set; }

        
    }
}