﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Editor3D.Entities.Models
{
    public class GNote
    {
        [Key]
        public int GNoteId { get; set; }
        public string Action { get; set; }
        public DateTime Time { get; set; }
        public string Description { get; set; }
    }
}